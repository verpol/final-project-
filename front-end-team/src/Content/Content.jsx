import React, {Component} from 'react';
import DatePicker from "../DatePicker";
import connect from "react-redux/es/connect/connect";
import "react-datepicker/dist/react-datepicker.css";
import IsWork from "../IsWork/IsWork";
import Exception from "../Exeption/Exception";
import Clock from "../Clock/Clock";
import Checkbox from "../Checkbox/Checkbox";
import AddExeptionButton from "../Clock/AddExeptionButton";
import ExtraClock from "../Clock/ExtraClock";
import './index.scss'
import Save from "../Save/Save";

class Content extends Component {
    constructor(props) {
        super(props);
        this.onDayClick = this.onDayClick.bind(this);
        this.nextMonth = this.nextMonth.bind(this);
        this.prevMonth = this.prevMonth.bind(this);
        this.onNotWorkClick = this.onNotWorkClick.bind(this);
        this.onWorkClick = this.onWorkClick.bind(this);
        this.addExeption = this.addExeption.bind(this);
        this.state = {
            selectedDate: new Date(),
            classForExtraClock: "clock_disappeared",
            getDayOfWeek: [],
            flag: true,
            flagIsLoad: false,
            timeFrom: "",
            timeTo: "",
            postMas: []
        };
    }

    k() {
        fetch('http://82.146.32.208:8080/work_days')
            .then(response => response.json())
            .then(data => {
                var tempmas = [];
                this.props.addTimeFrom(data[0].startTime);
                this.props.addTimeTo(data[0].endTime);
                var ty= [];
                for(var i = 0; i < data.length; i++) {
                    ty.push(data[i].weekday);
                    this.setState({
                        getDayOfWeek: [...this.state.getDayOfWeek, data[i].weekday],
                        flag: false
                    });
                }
                this.setState({
                    flagIsLoad: true,
                    timeFrom: data[0].startTime,
                    timeTo: data[0].endTime
                });

                this.props.workDayFromServer(ty);

            })
        this.setState({
            flag: false
        });

        fetch('http://82.146.32.208:8080/exclusion')
            .then(response => response.json() )
            .then(data => {
                for(var i = 0; i < data.length; i++) {

                    if(data[i].startTime === undefined) {
                        var arr = data[i].date.split('.');
                        var date = new Date(Number(arr[2]), Number(arr[1])-1, Number(arr[0]));
                        let temp = this.props.notWorkDay.map(a => String(a));
                        if (temp.indexOf(String(date)) == -1) {
                            this.props.onAdd_Not_Work_Day(date);
                        }
                    } else {
                        var arr = data[i].date.split('.');
                        var date = new Date(Number(arr[2]), Number(arr[1])-1, Number(arr[0]));
                        var t = [];
                        for(var j = 0; j < this.props.workDay.length; j++) {
                            t.push(String(this.props.workDay[j].checkedDay))
                        }
                        if (t.indexOf(String(date)) == -1) {
                            this.props.onAddWorkingDay(data[i].startTime, data[i].endTime, date);
                        }
                    }
                }
            })
        var te = [];
        for(var i = 0; i < this.props.standartWorkingDay; i++) {
            te.push({"weekday":this.props.standartWorkingDay[i],"startTime":"9:00","endTime":"16:00" })
        }
        /*fetch('http://82.146.32.208:8080/work_days', {
            method: 'post',
            body: JSON.stringify([

                {"weekday":"0","startTime":"9:00","endTime":"16:00"},

                {"weekday":"1","startTime":"9:00","endTime":"16:00"},
                {"weekday":"2","startTime":"9:00","endTime":"16:00"}]),
            headers: {
                'content-type': 'application/json'
            }
        })*/
    }


    render() {

        if(this.state.flag) {
            this.k();
        }
        console.log("основа")
        console.log(this.props.time);
        console.log(this.props.standartWorkingDay);
        console.log(this.props.workDay);
        console.log(this.props.notWorkDay);
        const {selectedDate} = this.state;
        /*fetch('https://api.openweathermap.org/data/2.5/weather?q=' + this.state.searchString + ',CA&appid=269180f147b6ac5a5ab5b421c1366e26')
            .then(response => response.json())
            .then(data => {
                if(data.cod == 404) {
                    this.props.dataHeader(
                        {
                            "temperature":null,
                            "feel":  null,
                            "name": null
                        }
                    )
                } else {
                    this.props.dataHeader(
                        {
                            "temperature": Math.round(parseFloat(data.main.temp) - 273.15),
                            "feel": data.weather[0].description,
                            "name": data.name
                        }
                    );
                }
            })*/
	    return (
		    <div className="content-wrapper">
			    <div className="App">
				    <div className="MainContent">
					    <div className="availabilityCalendar">My Availability Calendar</div>
					    <div className="workSchedule">
						    <div className="workScheduleTitle">Work Schedule</div>
						    <div className="titleWorkHours">Work Hours</div>
						    <Clock tf={this.state.timeFrom} tt={this.state.timeTo} fl={this.state.flagIsLoad} flag={true}/>
						    <Checkbox fl={this.state.flagIsLoad} m={this.state.getDayOfWeek}/>
						    <div className="titleOFExeptions">Exeptions</div>
						    <div className="calendarExeptions">
							    <DatePicker fullDate={selectedDate} onDayClick={this.onDayClick} nextMonth={this.nextMonth}
							                prevMonth={this.prevMonth}/>
							    <Exception/>
						    </div>
						    <IsWork onNotWorkClick={this.onNotWorkClick} onWorkClick={this.onWorkClick}/>
						    <ExtraClock seen={this.state.classForExtraClock} addExeption={this.addExeption}/>
						    <Save/>
					    </div>
				    </div>
			    </div>
		    </div>
	    )
    }

    onDayClick(newDay) {
        const {selectedDate} = this.state;
        this.setState({
            selectedDate: new Date(
                selectedDate.getFullYear(),
                selectedDate.getMonth(),
                newDay
            )
        });
    }

    onNotWorkClick() {
        if (this.state.selectedDate > new Date()) {
            let temp = this.props.notWorkDay.map(a => String(a));
            if (temp.indexOf(String(this.state.selectedDate)) !== -1) {
                this.props.Del_Not_Work(this.state.selectedDate);
            } else {
                this.props.onAdd_Not_Work_Day(this.state.selectedDate);
                let temp2 = this.props.workDay.map(a => String(a));
                if (temp2.indexOf(String(this.state.selectedDate)) !== -1) {
                    this.props.onAdd_Not_Work_Day(this.state.selectedDate);
                } else {
                    this.props.onDelWorkingDay(this.state.selectedDate);
                }
            }
        }
    }

    addExeption(timeFrom, timeTo) {
        this.props.onAddWorkingDay(timeFrom, timeTo, this.state.selectedDate);
    }

    onWorkClick() {
        if (this.state.selectedDate > new Date()) {
            if(this.state.classForExtraClock == "clock_disappeared") {
                this.setState({
                    classForExtraClock: "clock_appeared"
                });
            } else {
                this.setState({
                    classForExtraClock: "clock_disappeared"
                });
            }
        }
    }

    nextMonth() {
        const currentMonth = this.state.selectedDate.getMonth();
        const currentYear = this.state.selectedDate.getFullYear();
        if (currentMonth === 11) {
            this.currentMonth = -1;
            this.currentYear++;
        }
        this.setState({
            selectedDate: new Date(
                currentYear,
                currentMonth + 1,
                1
            )
        });
    }

    prevMonth() {
        const currentMonth = this.state.selectedDate.getMonth();
        const currentYear = this.state.selectedDate.getFullYear();
        if (currentMonth === 0) {
            this.currentMonth = 12;
            this.currentYear--;
        }
        this.setState({
            selectedDate: new Date(
                currentYear,
                currentMonth - 1,
                1
            )
        });
    }
}

export default connect(
    state => ({
        notWorkDay: state.notWorkingDay,
        workDay: state.workingDay,
        time: state.time,
        standartWorkingDay: state.standartWorkingDay,
    }),
    dispatch => ({
        onDelWorkingDay: (day) => {
            dispatch({type: 'onDelWorkingDay', day:day})
        },
        workDayFromServer: (mas) => {
            dispatch({type: 'workDayFromServer', mas:mas})
        },
        onAddStandartWorkingDay: (id) => {
            dispatch({type: 'onAddStandartWorkingDay', id:id})
        },
        onAdd_Not_Work_Day: (day) => {
            dispatch({type: 'Add_Not_Working_Day', day: day})
        },
        onAddWorkingDay: (timeFrom, timeTo, checkedDay) => {
            dispatch({type: 'onAddWorkingDay', timeFrom: timeFrom, timeTo: timeTo, checkedDay: checkedDay})
        },
        Del_Not_Work: (day) => {
            dispatch({type: 'Del_Not_Working_Day', day: day})
        },
        addTimeTo: (timeTo) => {
            dispatch({type: 'addTimeTo', timeTo: timeTo})
        },
        addTimeFrom: (timeFrom) => {
            dispatch({type: 'addTimeFrom', timeFrom: timeFrom})
        }
    })
)(Content)