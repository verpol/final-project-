import React, {Component} from "react";
import connect from "react-redux/es/connect/connect";
import "./DatePicker.scss";
class Day extends Component {



	render() {

		if(this.props.fullDate == null) {
			if(this.props.standartWorkingDay.indexOf(String(this.props.index)) !== -1) {
				return <div className="StandartDay--Work" />
			} else {
				return <div className="EmptyStateDay" />
			}
		}

		const date = this.props.fullDate.getDate();

		let className = "Day";


		if(this.props.standartWorkingDay.indexOf(String(this.props.index)) !== -1) {
			className = className + " StandartDay--Work";
		}


		if(( String(this.props.notWorkDay).indexOf(String(this.props.fullDate)) != -1) && (this.props.fullDate != undefined)) {
			if(this.props.fullDate > new Date()) {
				className = "Day Day--notWork";
			}
		}


		let result = this.props.workDay.map(a => String(a.checkedDay));
		if(( result.indexOf(String(this.props.fullDate)) != -1) && (this.props.fullDate != undefined)) {
			if(this.props.fullDate > new Date()) {
				className = "Day--Work";
			}
		}





		if (this.props.selected) {
			className = "Day--selected ";
		} else if (this.props.hovering) {
			className = className + " Day--hovering";
		}

		return (
			<button
				className={className}
				onClick={this.props.onClick.bind(this, date)}
				onMouseEnter={this.props.onMouseEnter.bind(this, date)}
				onMouseLeave={this.props.onMouseLeave.bind(this, date)}
			>
				{date}
			</button>
		);
	}
}

export default connect(
	state => ({
		standartWorkingDay: state.standartWorkingDay,
		notWorkDay: state.notWorkingDay,
		workDay: state.workingDay
	}),
	dispatch => ({
		onAddStandartWorkingDay: (id) => {
			dispatch({type: 'onAddStandartWorkingDay', id:id})
		},
		onDeleteStandartWorkingDay: (id) => {
			dispatch({type: 'onDeleteStandartWorkingDay', id:id})
		},

	})
)(Day)
