import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import "react-datepicker/dist/react-datepicker.css";
import './index.scss'

class IsWork extends Component {


    render() {
        return (
            <div className="buttons">
                <div className="not_work" onClick={this.props.onNotWorkClick}>
                    <div className="square"></div>
                    <div className="text">Not Work</div>
                </div>
                <div className="work" onClick={this.props.onWorkClick}>
                    <div className="square"></div>
                    <div className="text">Work</div>
                </div>
            </div>
        )
    }

}

export default connect(
    state => ({
        notWorkDay: state.notWorkingDay
    })
)(IsWork)