import React, {Component, Fragment} from 'react';
import './index.scss'

class AddExeptionButton extends Component {

	constructor(props) {
		super(props);
		this.onAddExeption = this.onAddExeption.bind(this);
		this.state = {
			timeFrom: '',
			timeTo: ''
		};
	}

	onAddExeption() {
		this.props.onAddExeption();
	}

	render(){
		return(
			<button onClick={this.onAddExeption} className="button">Add Exeption</button>
		);
	}
}

export default AddExeptionButton;