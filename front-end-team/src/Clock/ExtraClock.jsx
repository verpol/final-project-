import React, {Component, Fragment} from 'react';
import './index.scss'
import Clock from "./Clock";
import AddExeptionButton from "./AddExeptionButton";


class ExtraClock extends Component {

    constructor(props) {
        super(props);
        this.changeTimeTo = this.changeTimeTo.bind(this);
        this.changeTimeFrom = this.changeTimeFrom.bind(this);
        this.onAddExeption = this.onAddExeption.bind(this);
        this.state = {
            timeFrom: '',
            timeTo: ''
        };
    }

   changeTimeFrom(timeFrom) {
       this.setState({
           timeFrom: timeFrom
       });
   }

   changeTimeTo(timeTo) {
       this.setState({
           timeTo: timeTo
       });
   }

    onAddExeption() {
        this.props.addExeption(this.state.timeFrom, this.state.timeTo);
    }

    render(){
        return(
            <div className={this.props.seen}>
                <Clock  changeTimeFrom={this.changeTimeFrom} changeTimeTo={this.changeTimeTo}/>
                <AddExeptionButton onAddExeption={this.onAddExeption}/>
            </div>
        );
    }
}

export default ExtraClock;