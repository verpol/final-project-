import React, { Component } from 'react';
import connect from "react-redux/es/connect/connect";

class Clock extends Component {

    constructor(props) {
        super(props);
        this.state = {
            timeTo: this.props.tf,
            timeFrom: this.props.tf,
            flag: true
        };
    }

    handleChangeTimeFrom = (e) => {
        this.setState({ timeFrom: e.target.value });
        if(this.props.flag == true) {
            this.props.addTimeFrom(e.target.value);
        } else {
            this.props.changeTimeFrom(e.target.value);
        }
    };

    handleChangeTimeTo = (e) => {
        this.setState({ timeTo: e.target.value });
        if(this.props.flag == true) {
            this.props.addTimeTo(e.target.value);
        } else {
            this.props.changeTimeTo(e.target.value);
        }
    };

    standartClock() {
        this.setState({
            flag: false,
            timeFrom: String(this.props.time.timeFrom),
            timeTo: String(this.props.time.timeTo),

        });
    }

	render(){
		if(this.props.fl && this.state.flag) {
			this.standartClock();
		}
		return(
			<form className="form">
				{/*<p className="titleWorkHours">Work Hours</p>*/}
				<div className="inputs">
					<input type="time" id="time" placeholder="Enter The Time From" className="input-time" onChange={this.handleChangeTimeFrom} value={this.state.timeFrom} />
					<input type="time" id="time" placeholder="Enter The Time To" className="input-time" onChange={this.handleChangeTimeTo} value={this.state.timeTo} />
				</div>
				{/*<button onClick={this.handleSubmit} className="button">Save</button>*/}
			</form>
		);
	}
}

export default connect(
    state => ({
        standartWorkingDay: state.standartWorkingDay,
        notWorkDay: state.notWorkingDay,
        workDay: state.workingDay,
        time: state.time
    }),
    dispatch => ({
        addTimeTo: (timeTo) => {
            dispatch({type: 'addTimeTo', timeTo: timeTo})
        },
        addTimeFrom: (timeFrom) => {
            dispatch({type: 'addTimeFrom', timeFrom: timeFrom})
        }
    })
)(Clock)