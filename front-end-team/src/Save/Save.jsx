import React, {Component, Fragment} from 'react';
import connect from "react-redux/es/connect/connect";
import './index.scss'



class Save extends Component {
    constructor(props) {
        super(props);
        this.postData = this.postData.bind(this);
        this.state = {
        };
    }

   postData() {
       var temp = [];
       for(var i = 0; i < this.props.standartWorkingDay.length; i++) {
           temp.push({"weekday":this.props.standartWorkingDay[i],"startTime":String(this.props.time.timeFrom),"endTime":String(this.props.time.timeTo) })
       }

       fetch('http://82.146.32.208:8080/work_days', {
            method: 'post',
            body: JSON.stringify(temp),
            headers: {
                'content-type': 'application/json'
            }
        })
       var temp2 = [];
       for(var i = 0; i < this.props.workDay.length; i++) {
           if (this.props.workDay[i] !== undefined) {
               var month = this.props.workDay[i].checkedDay.getMonth();
               month++;
               var date = this.props.workDay[i].checkedDay.getDate() + "." + month +
                   "." + this.props.workDay[i].checkedDay.getFullYear();
               temp2.push({
                   "date": date,
                   "startTime": String(this.props.workDay[i].timeFrom),
                   "endTime": String(this.props.workDay[i].timeTo),
                   "isWork": 1
               })
           }
       }
       for(var i = 0; i < this.props.notWorkDay.length; i++) {
           var tempdata = this.props.notWorkDay[i].getDate() ;
           var month = this.props.notWorkDay[i].getMonth();
           month++;
           var date = tempdata + "." + month +
               "." + this.props.notWorkDay[i].getFullYear();
           temp2.push({"date":date,"startTime":null,"endTime":null, "isWork": true })
       }
       fetch('http://82.146.32.208:8080/exclusion', {
           method: 'post',
           body: JSON.stringify(temp2),
           headers: {
               'content-type': 'application/json'
           }
       })
   }

    render(){
	    return(
		    <div className="save_btn">
			    <button className="button_save" onClick={this.postData}>Save</button>
		    </div>
	    );
    }
}

export default connect(
    state => ({
        notWorkDay: state.notWorkingDay,
        workDay: state.workingDay,
        time: state.time,
        standartWorkingDay: state.standartWorkingDay,
    }),
    dispatch => ({
        workDayFromServer: (mas) => {
            dispatch({type: 'workDayFromServer', mas:mas})
        },
        onAddStandartWorkingDay: (id) => {
            dispatch({type: 'onAddStandartWorkingDay', id:id})
        },
        onAdd_Not_Work_Day: (day) => {
            dispatch({type: 'Add_Not_Working_Day', day: day})
        },
        onAddWorkingDay: (timeFrom, timeTo, checkedDay) => {
            dispatch({type: 'onAddWorkingDay', timeFrom: timeFrom, timeTo: timeTo, checkedDay: checkedDay})
        },
        Del_Not_Work: (day) => {
            dispatch({type: 'Del_Not_Working_Day', day: day})
        },
        addTimeTo: (timeTo) => {
            dispatch({type: 'addTimeTo', timeTo: timeTo})
        },
        addTimeFrom: (timeFrom) => {
            dispatch({type: 'addTimeFrom', timeFrom: timeFrom})
        }
    })
)(Save)