import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import "react-datepicker/dist/react-datepicker.css";

class Exception extends Component {

    render() {
        let mas = this.props.notWorkDay.map((n) => {
            return (
                <div>{String(n.getDate() + "." + Number(n.getMonth()+1) + "." + n.getFullYear() + " not work")}</div>
            )

        });

        let mas2 = this.props.workDay.map((n) => {

            return (
                <div>{String(n.checkedDay.getDate() + "."  +  Number(n.checkedDay.getMonth()+1) + "."  + n.checkedDay.getFullYear() +
                    " work from " + n.timeFrom + " to " + n.timeTo)}</div>
            )
        });

        return (
            <div >
                {mas}
                {mas2}
            </div>
        )
    }

}

export default connect(
    state => ({
        notWorkDay: state.notWorkingDay,
        workDay: state.workingDay
    })
)(Exception)