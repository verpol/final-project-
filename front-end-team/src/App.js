import React, {Component} from 'react';
import Header from "./adminLTE/Header";
import Menu from "./adminLTE/Menu";
import Footer from "./adminLTE/Footer";
import Content from "./Content/Content";

export default class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Menu/>
                <Content/>
                <Footer/>
            </div>
        )
    }
}


