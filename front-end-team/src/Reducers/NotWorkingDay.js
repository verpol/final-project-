export default function (notWorkingDay = [], action) {
    if (action.type === 'Add_Not_Working_Day') {
        return [
            ...notWorkingDay,
            action.day
        ];
    }

    function isEqualWrongDay(el) {
        let str = String(el.getDate()) + String(el.getMonth()) + String(el.getFullYear());
        if(String(action.day.getDate()) + String(action.day.getMonth()) + String(action.day.getFullYear()) !== str) {
            return el;
        }
    }

    if (action.type === 'Del_Not_Working_Day'){
        return notWorkingDay.filter(isEqualWrongDay)
    }
    return notWorkingDay;
}