export default function (time = {}, action) {
    if (action.type === 'addTimeTo') {
        return {
    ...
        time, timeTo: action.timeTo
    };
    }
    if (action.type === 'addTimeFrom') {
        return {
            ...
                time, timeFrom: action.timeFrom
        };
    }
    return time;
}