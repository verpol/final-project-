import {combineReducers} from "redux";
import NotWorkingDay from "./NotWorkingDay";
import WorkingDay from"./WorkingDays";
import Time from "./Time";

import StandartWorkingDay from "./StandartWorkingDay";


const allReducers = combineReducers ({
    notWorkingDay: NotWorkingDay,
    workingDay: WorkingDay,
    time: Time,
    standartWorkingDay: StandartWorkingDay
});


export default allReducers;