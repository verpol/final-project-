export default function (standartWorkingDay = [], action) {
	if (action.type === 'onDeleteStandartWorkingDay') {
		return standartWorkingDay.filter(el => el !== action.id);
	}
	if (action.type === 'onAddStandartWorkingDay') {
		return [
			...standartWorkingDay, action.id
		];
	}
	if(action.type === 'workDayFromServer') {
		var tempmas = [...standartWorkingDay];
		for(var i = 0; i < action.mas.length; i++) {
			tempmas = [...tempmas, String(action.mas[i])]
		}
		return tempmas
	}
	return standartWorkingDay;
}