export default function (workingDay = [], action) {
    if (action.type === 'onDelWorkingDay') {
        for(var i = 0; i < workingDay.length; i++) {
            if(String(workingDay[i].checkedDay) == String(action.day)) {
                delete workingDay[i];
            }
        }
        return workingDay;
    }
    if (action.type === 'onAddWorkingDay') {



        return [
            ...workingDay, {timeFrom: action.timeFrom, timeTo: action.timeTo, checkedDay: action.checkedDay}
        ];
    }
    return workingDay;
}