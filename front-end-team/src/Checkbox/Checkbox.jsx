import React, { Component } from 'react';
import './index.scss'
import connect from "react-redux/es/connect/connect";

class Checkbox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: [false, false, false, false, false, false, false],
			flag: true
        };
    }

	func(ev) {
		if (this.props.standartWorkingDay.indexOf(String(ev.target.id)) !== -1) {
			this.props.onDeleteStandartWorkingDay(ev.target.id);
		} else {
			this.props.onAddStandartWorkingDay(ev.target.id);
		}
	}

	changeIsChecked(ev) {
        let tempmas = this.state.isChecked;
        tempmas[ev.target.id] = !this.state.isChecked[ev.target.id]
	}

  standartWorkDay() {
    	let tempmas = this.state.isChecked;
    	for(var i = 0; i < this.props.m.length; i++) {
    		tempmas[this.props.m[i]] = true;
		}
      this.setState({
		  isChecked: tempmas,
          flag: false
      });
  }

	render() {
    	if(this.props.fl && this.state.flag) {
    		this.standartWorkDay();
		}
		return (
			<div className="workDays" >
				<div>
					<p className="titleWorkDays">Work Days</p>
				</div>
				<div className="listWorkDays">
					<div className="itemWorkDays">
						<label htmlFor="mon" className="dayName">Mon</label>
						<input type="checkbox" id="0" name="Monday" onChange={this.changeIsChecked.bind(this)} onClick={this.func.bind(this)} checked={this.state.isChecked[0]}/>
					</div>
					<div className="itemWorkDays">
						<label htmlFor="tue" className="dayName">Tue</label>
						<input type="checkbox" id="1" name="Tuesday" onChange={this.changeIsChecked.bind(this)} onClick={this.func.bind(this)} checked={this.state.isChecked[1]}/>
					</div>
					<div className="itemWorkDays">
						<label htmlFor="wed" className="dayName">Wed</label>
						<input type="checkbox" id="2" name="Wednesday" onChange={this.changeIsChecked.bind(this)} onClick={this.func.bind(this)} checked={this.state.isChecked[2]}/>
					</div>
					<div className="itemWorkDays">
						<label htmlFor="thu" className="dayName">Thu</label>
						<input type="checkbox" id="3" name="Thursday" onChange={this.changeIsChecked.bind(this)} onClick={this.func.bind(this)} checked={this.state.isChecked[3]}/>
					</div>
					<div className="itemWorkDays">
						<label htmlFor="fri" className="dayName">Fri</label>
						<input type="checkbox" id="4" name="Friday" onChange={this.changeIsChecked.bind(this)} onClick={this.func.bind(this)} checked={this.state.isChecked[4]}/>
					</div>
					<div className="itemWorkDays">
						<label htmlFor="sat" className="dayName">Sat</label>
						<input type="checkbox" id="5" name="Saturday" onChange={this.changeIsChecked.bind(this)} onClick={this.func.bind(this)} checked={this.state.isChecked[5]}/>
					</div>
					<div className="itemWorkDays">
						<label htmlFor="sun" className="dayName">Sun</label>
						<input type="checkbox" id="6" name="Sunday" onChange={this.changeIsChecked.bind(this)} onClick={this.func.bind(this)} checked={this.state.isChecked[6]}/>
					</div>
				</div>
			</div>
		);
	}
}

export default connect(
	state => ({
		standartWorkingDay: state.standartWorkingDay
	}),
	dispatch => ({
		onAddStandartWorkingDay: (id) => {
			dispatch({type: 'onAddStandartWorkingDay', id:id})
		},
		onDeleteStandartWorkingDay: (id) => {
			dispatch({type: 'onDeleteStandartWorkingDay', id:id})
		}
	})
)(Checkbox)

